﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NinjaMvc.Model;
using NinjaMvc.Repository;

namespace NinjaMvc.Controllers
{
    public class HomeController : Controller
    {
        private INinjaRepository _ninjaRepository;

        public HomeController(INinjaRepository ninjaRepository)
        {
            _ninjaRepository = ninjaRepository;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            var ninja = new Ninja{ Name="Zohaib",PhoneNumber ="0343-70606331"};
            _ninjaRepository.Add(ninja);
            ViewData["Message"] = "New Ninja Added";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
