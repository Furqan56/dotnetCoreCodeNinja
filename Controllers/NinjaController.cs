using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NinjaMvc.Repository;

namespace NinjaMvc.Controllers
{
    public class NinjaController:Controller{
        private INinjaRepository _ninjaRepository;

        public NinjaController(INinjaRepository ninjaRepository)
        {
            _ninjaRepository = ninjaRepository;
        }
        public ActionResult Index()
        {
            var ninjas=_ninjaRepository.GetNinjas();
            return View(ninjas);    
        }
    }
}