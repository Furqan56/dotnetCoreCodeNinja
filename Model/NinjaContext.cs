
using Microsoft.EntityFrameworkCore;

namespace NinjaMvc.Model
{
    public class NinjaContext:DbContext
    {
        public DbSet<Ninja> Ninjas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder){
            optionBuilder.UseSqlite("Filename=./mydb1.db");
        }
    }
}