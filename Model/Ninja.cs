using System;
using System.ComponentModel.DataAnnotations;

namespace NinjaMvc.Model
{
    public class Ninja
    {
        public Ninja()
        {
            Id= Guid.NewGuid().ToString();
        }
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}