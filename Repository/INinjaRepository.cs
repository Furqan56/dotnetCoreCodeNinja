using System.Collections.Generic;
using NinjaMvc.Model;

namespace NinjaMvc.Repository
{
    public interface INinjaRepository
    {
        List<Ninja> GetNinjas();
        void Add(Ninja ninja);
    }
}