using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using NinjaMvc.Model;

namespace NinjaMvc.Repository
{
    public class NinjaRepository:INinjaRepository
    {
        private NinjaContext _context;

        public NinjaRepository(NinjaContext context)
        {
            _context = context;
        }

        public List<Ninja> GetNinjas(){
          return  _context.Ninjas.ToList();
        }

        public void Add(Ninja ninja){
            _context.Ninjas.Add(ninja);
            _context.SaveChanges();
        }
    }
}